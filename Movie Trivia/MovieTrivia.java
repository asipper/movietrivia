import java.applet.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Goal: Fully functioning movie trivia game
 * with a minimum of 5 questions, multiple
 * choice style, that uses the Question class
 * Scoring, rating, etc. makes the game more
 * realistic and FUN!
 * 
 * @author (Andrew Sipper) 
 * @version (2/9/16)
 */
public class MovieTrivia extends Applet implements ItemListener, ActionListener
{
    private Image currentImage;
    private AudioClip song; // much later
    private Checkbox cb1, cb2, cb3;
    private CheckboxGroup cbg;
    private int right, wrong, remaining, quizSize;
    private Label titleLabel, responseLabel, scoreLabel, rightLabel, remainingLabel,
        wrongLabel, rightNum, wrongNum, remainingNum;
    private Question[] qList;
    private Question currentQuestion;
    private Font bigFont, regFont;
    private Button submit, next, play;
    private AudioClip currentAudio;
    private boolean isQuizStarted;
    private int quizNum;
    
    public void init()
    {
        bigFont = new Font("Ariel", Font.BOLD, 36);
        regFont = new Font("Ariel", Font.BOLD, 18);
        
        setLayout(null); 
        // Clears any autoformating built in, and we have to specify
        // the exact locations and sizes of each component
        
        titleLabel = new Label("Movie Trivia Quiz");
        titleLabel.setFont(bigFont);
        titleLabel.setLocation(20,20);
        titleLabel.setSize(300,36);
        titleLabel.setBackground(Color.green);
        titleLabel.setForeground(Color.blue);
        add(titleLabel);
        
        cbg = new CheckboxGroup();
        
        cb1 = new Checkbox("Choice 1", cbg, false);
        cb1.setFont(regFont);
        cb1.setLocation(50,300);
        cb1.setSize(250,30);
        cb1.addItemListener(this);
        
        cb2 = new Checkbox("Choice 2", cbg, false);
        cb2.setFont(regFont);
        cb2.setLocation(50,330);
        cb2.setSize(250,30);
        cb2.addItemListener(this);
        
        cb3 = new Checkbox("Choice 3", cbg, false);
        cb3.setFont(regFont);
        cb3.setLocation(50,360);
        cb3.setSize(250,30);
        cb3.addItemListener(this);
        
        //add(cb1); add(cb2); add(cb3);
        
        quizSize = 5;
        qList = new Question[quizSize];
         // won't work until we
        // write this method!
        
        play = new Button("Play Game");
        play.setLocation(20,250);
        play.setSize(100,20);
        play.addActionListener(this);

        add(play);
        
        submit = new Button("Submit");
        submit.setLocation(120,250);
        submit.setSize(100,20);
        submit.addActionListener(this);
        submit.setEnabled(false);
        add(submit);
        
        next = new Button("Next");
        next.setLocation(220,250);
        next.setSize(100,20);
        next.addActionListener(this);
        next.setEnabled(false);
        add(next);
        
        responseLabel = new Label("Will you get it?");
        responseLabel.setLocation(20,400);
        responseLabel.setSize(250,30);
        responseLabel.setFont(regFont);
        add(responseLabel);
        
        rightLabel = new Label("Right:");
        rightLabel.setLocation(300,400);
        rightLabel.setSize(130,30);
        rightLabel.setFont(regFont);
        
        wrongLabel = new Label("Wrong:");
        wrongLabel.setLocation(300,430);
        wrongLabel.setSize(130,30);
        wrongLabel.setFont(regFont);
        
        remainingLabel = new Label("Remaining:");
        remainingLabel.setLocation(300,460);
        remainingLabel.setSize(130,30);
        remainingLabel.setFont(regFont);
        
        rightNum = new Label("0");
        rightNum.setLocation(430,400);
        rightNum.setSize(30,30);
        rightNum.setFont(regFont);
        
        wrongNum = new Label("0");
        wrongNum.setLocation(430,430);
        wrongNum.setSize(30,30);
        wrongNum.setFont(regFont);
        
        remainingNum = new Label("0");
        remainingNum.setLocation(430,460);
        remainingNum.setSize(30,30);
        remainingNum.setFont(regFont);
        
        add(rightLabel); add(wrongLabel); add(remainingLabel);
        add(rightNum); add(wrongNum); add(remainingNum);
        
        quizNum = 0;
        currentAudio = null;        
        isQuizStarted = false;
        music();
    }
    
    public void buildQuestions()
    {
         // goal - is to fill the Question
         // template with what it needs, making
         // 5 variables of type Question, then
         // adding them to qList       
         
         if (quizNum == 1)
         {
             String f1 = "shining.png"; // file name
             String[] a1 = {"The Shining", "A Few Good Men",
                 "Anger Management"}; // answer choices
             int i1 = 0; //0th index is correct for a1 & f1
             Question q1 = new Question(f1,a1,i1);
             qList[0] = q1;
             
             String f2 = "independence.png";
             String[] a2 = {"Armegedden", "Men In Black",
                "Independence Day"};
             int i2 = 2;
             Question q2 = new Question(f2,a2,i2);
             qList[1] = q2;
             
             String f3 = "forrest.png";
             String[]a3 ={"Forrest Gump","Apollo 13","Big"};
             int i3 = 0;
             Question q3 = new Question(f3,a3,i3);
             qList[2] = q3;
             
             String f4 = "vegas.png";
             String[] a4 = {"European Vacation", 
                 "Vegas Vacation", "Christmas Vacation"};
             int i4 = 1;
             Question q4 = new Question(f4,a4,i4);
             qList[3] = q4;
             
             String f5 = "dark.png";
             String[] a5 = {"Batman and Robin", "Dark Knight",
                 "Batman Begins"};
             int i5 = 1;
             Question q5 = new Question(f5, a5, i5);
             qList[4] = q5;
         }
         if (quizNum == 2)
         {
             // you write 5 more questions
             String f6 = "taxidriver.jpg";
             String[] a6 = {"Taxi Driver", "Frozen", "Independence Day"};
             int i6 = 0;
             Question q6 = new Question(f6, a6, i6);
             qList[0] = q6;
             
             String f7 = "1024.basterds.ls.12412_copy.jpg"; // inglorious basterds
             String[] a7 = {"Pulp Fiction", "Inglorious ********", "Resevoir Dogs"};
             int i7 = 1;
             Question q7 = new Question(f7,a7,i7);
             qList[1] = q7;
             
             String f8 = "ffc39ebe6c87b384c277ca8ce3a63ba4.jpg"; // Pulp Fiction
             String[] a8 = {"Pulp Fiction", "Inglorious ********", "Resevoir Dogs"};
             int i8 = 0;
             Question q8 = new Question(f8,a8,i8);
             qList[2] = q8;
             
             String f9 = "wafflebot.jpg";
             String[] a9 = {"Harold and Kumar Escape from Guantamno Bay", "Harold and Kumar Christmas", "Harold and Kumar Go to White Castle"};
             int i9 = 1;
             Question q9 = new Question(f9,a9,i9);
             qList[3] = q9;
             
             String f10 = "south-park-bigger-longer-and-uncut-1999.jpg";
             String[] a10 = {"The Simpsons Movie", "The Family Guy Movie", "South Park: Bigger Longer Uncut"};
             int i10 = 2;
             Question q10 = new Question(f10,a10,i10);
             qList[4] = q10;
         }  
    }
    
    public void paint(Graphics g)
    {
        g.drawImage(currentImage,20,50,this);
        g.drawRect(290,392,170,106);
        
        // award stars when game is over (remaining = 0)
        if (remaining == 0)
        {
            int x = 5, y = 425;
            Image gs = getImage(getCodeBase(),"goldstar.png");
            Image ws = getImage(getCodeBase(),"whitestar.png");
            // gold stars first, white stars second
            for (int i = 0; i < right; i++)
            {
                g.drawImage(gs,x,y,this);
                x+=55; // short for x = x + 60
            }
            for (int i = 0; i < wrong; i++)
            {
                g.drawImage(ws,x,y,this);
                x+=55; // short for x = x + 60
            }
        }
        
    }
    
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == play)
        {
            play.setEnabled(false); // turns off the play
            // button so you can't accidently (or purposely)
            // reset the game
            
            // assume we have 2 quizzes available to us
            currentAudio.stop();
            quizNum++;
           // if (quizNum == 3) // we've done both quizzes now want 3rd
              //  quizNum = 1;
             currentAudio =  getAudioClip ( getCodeBase(), "Jeopardy_Music.wav");
             
            startGame(); // calls a method that will
            // set the initial conditions of the game
            
        }
        if (e.getSource() == next)
        {
            next.setEnabled(false);
            submit.setEnabled(true);
            
            currentQuestion = qList[quizSize - remaining];
            displayQuestion(); // updates display of current
            // question
        }
        if (e.getSource() == submit)// check if you are right
        {
            if (cb1.getState() == false &&
                cb2.getState() == false &&
                cb3.getState() == false)
            {
                responseLabel.setText("Take a guess!");
            }
            else
            {
                remaining--; // subtracts 1 from variable
                // remaining, also known as decrementing variable
                submit.setEnabled(false);
                next.setEnabled(true);
                // let's figure out what was guessed
                int index = 0;
                if (cb2.getState())
                    index = 1;
                if (cb3.getState())
                    index = 2;
                if (currentQuestion.checkAnswer(index) == true) // right!
                {
                    responseLabel.setText("Correct!");
                    responseLabel.setForeground(Color.green);
                    AudioClip pos = getAudioClip ( getCodeBase(), "cheering.wav");
                    pos.play();
                    right++;
                }
                else
                {
                    responseLabel.setText("Good try, but no!");
                    responseLabel.setForeground(Color.red);
                    wrong++;
                    AudioClip neg = getAudioClip ( getCodeBase(), "cough_x.wav");
                    neg.play();
                }                
                updateScore(); // write this to update score view                
                if (remaining == 0)
                {
                    play.setEnabled(true);
                    currentQuestion = null;
                    submit.setEnabled(false);
                    next.setEnabled(false);
                    currentAudio.stop();
                    currentAudio =  getAudioClip ( getCodeBase(), "songbird.wav"); 
                    currentAudio.play();
                    
                }
            }            
        }        
        repaint();
    }
    
    public void updateScore()
    {        
        rightNum.setText("" + right);
        wrongNum.setText("" + wrong);
        remainingNum.setText("" + remaining);
    }
    
    public void itemStateChanged(ItemEvent e)
    {   
        repaint();
    }
    public void music()
    {
      
        
            currentAudio =  getAudioClip ( getCodeBase(), "songbird.wav"); 
        
        
        currentAudio.play();
    }
    public void startGame()
    {
        right = 0;
        wrong = 0;
        remaining = quizSize;
        buildQuestions();
        // Checkboxes initially off the viewer because we don't want to
        // be able to guess an answer choice before there is a question
        add(cb1); add(cb2); add(cb3);
        submit.setEnabled(true);
        
        currentQuestion = qList[0];
        displayQuestion(); // not written yet
        isQuizStarted = true;
        currentAudio.loop();
    }
    
    public void displayQuestion()
    {
        currentImage = getImage(getCodeBase(),
                        currentQuestion.getPicFile());
        String[] aList = currentQuestion.getAnsArray();
        
        cb1.setCheckboxGroup(null);
        cb2.setCheckboxGroup(null);
        cb3.setCheckboxGroup(null);
        
        cb1.setState(false);
        cb2.setState(false);
        cb3.setState(false);
        
        cb1.setCheckboxGroup(cbg);
        cb2.setCheckboxGroup(cbg);
        cb3.setCheckboxGroup(cbg);
        
        cb1.setLabel(aList[0]);
        cb2.setLabel(aList[1]);
        cb3.setLabel(aList[2]);
        responseLabel.setText("Will you get it?");
        responseLabel.setForeground(Color.black);
        updateScore();
        //currentAudio = null;
        //currentAudio =  getAudioClip ( getCodeBase(), "Jeopardy_Music.wav");
        //currentAudio.play();
    }
}
