
/**
 * Intro to Object Oriented Programming
 * We are building a class called Question
 * that will serve as a template for all 
 * of our questions in our trivia app
 * 
 * When complete, we can make variables of 
 * type Question that can be grouped and
 * presented as a quiz. There should be a
 * way of telling if the player answers
 * correctly
 * 
 * @author (Wes Tion) 
 * @version (Feb 9)
 */
public class Question
{
    // standard class structure
    // fields, then constructors, then methods
    // fields - what are the elements of 
    // of Question?
    
    private String picFile;
    private String[] ansArray;
    private int correctIndex;
    
    // constructor - sets notation for
    // making variables of type Question
    // whatever a client sends, those values
    // are assigned to our fields
    public Question(String p, String[] a, int i)
    {
        picFile = p;
        ansArray = a;
        correctIndex = i; 
    }
    
    // write some accessor methods to give 
    // client classes access to Question's data
    public String getPicFile()
    {   return picFile;     }
    
    public String[] getAnsArray()
    {   return ansArray;    }
    
    public int getCorrectIndex()
    {   return correctIndex;    }
        
    public boolean checkAnswer(int guess)
    {
        if (guess == correctIndex)
            return true;
        else
            return false;
    }        
}
